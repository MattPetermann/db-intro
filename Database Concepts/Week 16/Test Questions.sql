 --Order 10977 contained items for another order shipped that same day. 
	--Write a query that identifies the orders shipped that day. Include 
	--the OrderID, OrderDate, customer ID & name, contact name & Phone 
	--so NW Traders can contact the customers about their orders.
SELECT o.OrderID, o.OrderDate, o.CustomerID, c.ContactName, c.Phone
FROM Orders o
JOIN Customers c
ON o.CustomerID = c.CustomerID
WHERE o.ShippedDate = (
	SELECT o.ShippedDate
	FROM Orders o
	WHERE o.OrderID = 10977
) AND o.OrderID != 10977;

--Create a scalar function that returns the number of items on a given order.
CREATE FUNCTION numItems (@orderID INT)
RETURNS INT AS
BEGIN
	DECLARE @return INT

	SELECT @return = SUM(od.Quantity)
	FROM OrderDetails od
	WHERE od.OrderID = @orderID

	RETURN @return
END

--Create a stored procedure that accepts two date parameters and returns all 
	--orders for that date range. Include information about the order, 
	--including the order total, as well as the Customer name, and the shipping company used.
CREATE PROCEDURE ordersInRange(@min DATE, @max DATE) AS
BEGIN
	SELECT o.OrderID, o.OrderDate, c.ContactName, s.CompanyName [Shipper], 
		CONVERT(DECIMAL(6,2), SUM(od.UnitPrice * od.Quantity * (1 - od.Discount))) [Order Total]
	FROM Orders o
	JOIN Customers c
	ON o.CustomerID = c.CustomerID
	JOIN Shippers s
	ON o.ShipVia = s.ShipperID
	JOIN OrderDetails od
	ON o.OrderID = od.orderID
	WHERE o.OrderDate >= @min AND o.OrderDate <= @max
	GROUP BY o.OrderID, o.OrderDate, c.ContactName, s.CompanyName
END;

CREATE PROCEDURE ordersInMonth(@date DATE) AS
BEGIN
	SELECT o.OrderID, o.OrderDate, c.ContactName, s.CompanyName [Shipper], 
		CONVERT(DECIMAL(6,2), SUM(od.UnitPrice * od.Quantity * (1 - od.Discount))) [Order Total]
	FROM Orders o
	JOIN Customers c
	ON o.CustomerID = c.CustomerID
	JOIN Shippers s
	ON o.ShipVia = s.ShipperID
	JOIN OrderDetails od
	ON o.OrderID = od.orderID
	WHERE MONTH(o.orderDate) = MONTH(@date) AND YEAR(o.OrderDate) = YEAR(@date)
	GROUP BY o.OrderID, o.OrderDate, c.ContactName, s.CompanyName
END;

--Create a role called "Test4Role" in your database. Add your _TestUser account to this role.
CREATE ROLE Test4Role;

ALTER ROLE Test4Role
ADD MEMBER mpetermann2_TestUser;

--Grant the Test4Role permission to run the stored procedure created in #23. 
GRANT EXEC ON ordersInRange TO Test4Role;

--Allow the Test4Role to select from all tables in NW_Traders, and allow the 
	--role to modify data in the Shippers table.
GRANT SELECT TO Test4Role;

GRANT ALTER ON Shippers TO Test4Role;