--1
CREATE INDEX Ix_Customer_Names ON Customers(CompanyName, ContactName);

--2
CREATE INDEX Ix_Shippers ON Shippers(CompanyName);

--3
DROP INDEX Ix_Shippers ON Shippers

--4
CREATE INDEX Ix_Postal_Codes ON Customers(PostalCode);

--6
CREATE INDEX Ix_Order_Dates ON Orders(OrderDate)
INCLUDE (ShippedDate, CustomerID, EmployeeID)