-- Create the tables --

CREATE TABLE Students (
	StudentID SMALLINT IDENTITY(1,1),
	StudentName VARCHAR(50) NOT NULL,

	PRIMARY KEY (StudentID)
);

CREATE TABLE EnrollmentStatus (
	StatusCode CHAR(1),
	StatusDescription VARCHAR(10) NOT NULL,

	PRIMARY KEY (StatusCode)
);

CREATE TABLE Semesters (
	SemesterCode CHAR(7),
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NOT NULL,

	PRIMARY KEY (SemesterCode)
);

CREATE TABLE Locations (
	LocationID SMALLINT IDENTITY(1,1),
	Location VARCHAR(4) NOT NULL,

	PRIMARY KEY (LocationID)
);

CREATE TABLE Instructors (
	InstructorID SMALLINT IDENTITY(1,1),
	InstructorName VARCHAR(50) NOT NULL,

	PRIMARY KEY (InstructorID)
);

CREATE TABLE Courses (
	CourseID SMALLINT IDENTITY(1,1),
	CourseNumber SMALLINT NOT NULL,
	CourseTitle VARCHAR(50) NOT NULL,
	SemesterCode CHAR(7) NOT NULL,
	LocationID SMALLINT NOT NULL,
	InstructorID SMALLINT NOT NULL,

	PRIMARY KEY (CourseID),
	CONSTRAINT fk_semesterCode FOREIGN KEY (SemesterCode)
		REFERENCES Semesters(SemesterCode),
	CONSTRAINT fk_locationID FOREIGN KEY (LocationID)
		REFERENCES Locations(LocationID),
	CONSTRAINT fk_instructorID FOREIGN KEY (InstructorID)
		REFERENCES Instructors(InstructorID)
);

CREATE TABLE StudentEnrollment (
	CourseID SMALLINT NOT NULL,
	StudentID SMALLINT NOT NULL,
	StatusCode CHAR(1) NOT NULL,
	
	CONSTRAINT fk_CourseID FOREIGN KEY (CourseID)
		REFERENCES Courses(CourseID),
	CONSTRAINT fk_studentID FOREIGN KEY (StudentID)
		REFERENCES Students(StudentID),
	CONSTRAINT fk_statusCode FOREIGN KEY (StatusCode)
		REFERENCES EnrollmentStatus(StatusCode)
);

-- Flood with data --

INSERT INTO Students VALUES (
	'Bessie Heedles'
);

INSERT INTO Students VALUES (
	'Sean Pineda'
);

INSERT INTO Students VALUES (
	'Gerard Biers'
);

INSERT INTO Students VALUES (
	'George Kocka'
);

INSERT INTO EnrollmentStatus VALUES (
	'E', 'Enrolled'
);

INSERT INTO EnrollmentStatus VALUES (
	'D', 'Dropped'
);

INSERT INTO Semesters VALUES (
	'2017_SP', '2/1/2017', '5/24/2017'
);

INSERT INTO Locations VALUES (
	'L210'
);

INSERT INTO Instructors VALUES (
	'Gary Pertez'
);

INSERT INTO Courses VALUES (
	204, 'Intro to SQL', '2017_SP', 1, 1
);

INSERT INTO StudentEnrollment VALUES (
	3, 1, 'E'
);

INSERT INTO StudentEnrollment VALUES (
	3, 2, 'E'
);

INSERT INTO StudentEnrollment VALUES (
	3, 3, 'D'
);

INSERT INTO StudentEnrollment VALUES (
	3, 4, 'E'
);

-- Drop tables --

ALTER TABLE Courses
DROP CONSTRAINT fk_semesterCode;
ALTER TABLE Courses
DROP CONSTRAINT fk_locationID;
ALTER TABLE Courses
DROP CONSTRAINT fk_instructorID;
DROP TABLE Courses;

ALTER TABLE StudentEnrollment
DROP CONSTRAINT fk_courseID;
ALTER TABLE StudentEnrollment
DROP CONSTRAINT fk_studentID;
ALTER TABLE StudentEnrollment
DROP CONSTRAINT fk_statusCode;
DROP TABLE StudentEnrollment;

DROP TABLE Students;

DROP TABLE EnrollmentStatus;

DROP TABLE Semesters;

DROP TABLE Locations;

DROP TABLE Instructors;