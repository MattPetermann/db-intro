--Find price of of Chocolade
SELECT p.UnitPrice
FROM Products p
WHERE p.ProductName = 'Chocolade'

--Find prices less than Chocolade
SELECT p.ProductName, p.UnitPrice
FROM Products p
WHERE p.UnitPrice < 12.75

--Find same info with subquery
SELECT p.ProductName, p.UnitPrice
FROM Products p
WHERE p.UnitPrice < (
	SELECT p.UnitPrice
	FROM Products p
	WHERE p.ProductName = 'Chocolade'
)

--Other products on the same orders as Chocolade
SELECT DISTINCT p.ProductName
FROM Products p
JOIN OrderDetails od
ON p.ProductID = od.ProductID
WHERE od.OrderID IN (
	SELECT od.OrderID
	FROM Products p
	JOIN OrderDetails od
	ON p.ProductID = od.ProductID
	WHERE p.ProductName = 'Chocolade'
)
AND p.ProductName != 'Chocolade'

--Display the products and the order total for order 10248
	--Find the order total
	--Find each product on the order
SELECT p.ProductName, od.UnitPrice, od.Quantity, od.Discount, (
	SELECT SUM(UnitPrice * Quantity * (1 - Discount))
	FROM OrderDetails od2
	WHERE od2.OrderID = od.OrderID
	GROUP BY od2.OrderID
) [Order Total]
FROM OrderDetails od
JOIN Products p
ON od.ProductID = p.ProductID
WHERE od.OrderID = 10248