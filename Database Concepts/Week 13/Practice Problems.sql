
--1.	Determine which orders were shipped to the same city as order 10958.
SELECT o.OrderID, o.ShipCity
FROM Orders o
WHERE o.ShipCity = (
	SELECT o.ShipCity
	FROM Orders o
	WHERE o.OrderID = 10958
) AND o.OrderID != 10958

--2.	List the Company name(s) of the Customer(s) for the most recent shipped date.
SELECT c.CompanyName
FROM Orders o
JOIN Customers c
ON c.CustomerID = o.CustomerID
WHERE o.ShippedDate = (
	SELECT TOP(1) o.ShippedDate
	FROM Orders o
	ORDER BY o.ShippedDate DESC
)

--3.	Which supplier(s) have only discontinued products? Provide the supplier information.
SELECT s.*
FROM Suppliers s
JOIN (
	SELECT s.SupplierID, COUNT(s.SupplierID) [DiscProducts]
	FROM Suppliers s
	JOIN Products p
	ON s.SupplierID = p.SupplierID
	WHERE p.Discontinued = 1
	GROUP BY s.SupplierID
) dp
ON s.SupplierID = dp.SupplierID
JOIN (
	SELECT s.SupplierID, COUNT(p.ProductID) [TotalProducts]
	FROM Suppliers s
	JOIN Products p
	ON s.SupplierID = p.SupplierID
	GROUP BY s.SupplierID
) tp
ON s.SupplierID = tp.SupplierID
WHERE dp.DiscProducts = tp.TotalProducts

--4.	Determine which products cost less than the average cost of other products in the same category.
SELECT p.ProductName, p.UnitPrice
FROM Products p
WHERE p.UnitPrice < (
	SELECT AVG(p.UnitPrice)
	FROM Products p
)

--5.	Identify if there are any orders that have not been shipped that were placed with 
	--an employee who no longer works for Northwind Traders. Provide the Order ID, Order Total 
	--and Contact information for the customer for follow up.
SELECT o.OrderID, c.ContactName, c.Phone, 
	CONVERT(DECIMAL(10,2), 
	(SUM(od.Quantity * od.UnitPrice * (1 - od.Discount)))) [Order Total]
FROM Orders o
JOIN OrderDetails od
ON o.OrderID = od.OrderID
JOIN Customers c
ON o.CustomerID = c.CustomerID
WHERE o.OrderID IN (
	SELECT o.OrderID
	FROM Orders o
	WHERE o.ShippedDate IS NULL
)
AND o.EmployeeID IN (
	SELECT e.EmployeeID
	FROM Employees e
	WHERE e.TerminationDate IS NOT NULL
)
GROUP BY o.OrderID, c.ContactName, c.Phone

--6.	Identify discontinued items that are out of stock. 
	--Determine which orders cannot be fulfilled due to this 
	--and provide order and customer information for customer 
	--service to follow up with each customer.
SELECT o.OrderID, c.CompanyName, c.ContactName, c.Phone
FROM OrderDetails od
JOIN Orders o
ON o.OrderID = od.OrderID
JOIN Customers c
ON o.CustomerID = c.CustomerID
WHERE od.ProductID IN (
	SELECT p.ProductID
	FROM Products p
	WHERE p.Discontinued = 1
	AND p.UnitsInStock = 0
)