--1)	Display the count of customers by country
SELECT c.Country, COUNT(c.CustomerID) [Customers]
FROM Customers c
GROUP BY c.Country

--2)	Display the total sales and total items sold by country
SELECT o.ShipCountry, COUNT(o.OrderID) [Orders], SUM(od.Quantity) [Number of Items]
FROM Orders o
JOIN OrderDetails od
ON o.OrderID = od.OrderID
GROUP BY o.ShipCountry
ORDER BY o.ShipCountry;

--3)	How much revenue has Outback Lager generated?
SELECT od.ProductID, CONVERT(DECIMAL(10,2), SUM(od.UnitPrice * od.Quantity*(1 - od.Discount))) [Revenue]
FROM OrderDetails od
WHERE od.ProductID = 70
GROUP BY od.ProductID;

--4)	Which product sold the most units in 2014?
SELECT TOP(1) od.ProductID, SUM(od.Quantity) [Items Sold 2014]
FROM OrderDetails od
JOIN Orders o
ON od.OrderID = o.OrderID
WHERE YEAR(o.OrderDate) = 2014
GROUP BY od.ProductID
ORDER BY SUM(od.Quantity) DESC;

--5)	Which countries have more than 100 orders?
CREATE VIEW OrderCount AS
SELECT o.ShipCountry, COUNT(o.ShipCountry) [Order Count]
FROM Orders o
GROUP BY o.ShipCountry;

SELECT *
FROM OrderCount
WHERE [Order Count] > 100;