--1.	Create and execute a stored procedure to return orders that have not shipped yet.
CREATE PROCEDURE ordersNotShipped AS
BEGIN
	SELECT o.OrderID
	FROM Orders o
	WHERE o.ShippedDate IS NULL
END

EXEC ordersNotShipped

--2.	Create and execute a stored procedure that returns the products that are sold 
	--and how many units of each that have been sold, including products that have no units sold.
CREATE PROCEDURE itemsSold AS
BEGIN
	SELECT p.productID, p.productName, SUM(od.Quantity) [Number Sold]
	FROM Products p
	LEFT JOIN OrderDetails od
	ON p.ProductID = od.ProductID
	GROUP BY p.productID, p.productName
END

EXEC itemsSold

--3.	Modify the stored procedure in #2 to accept a category parameter to display 
	--only products for the passed in parameter. Display the results for the Confections category.
ALTER PROCEDURE itemsSold (@category VARCHAR(15)) AS
BEGIN
	SELECT p.productID, p.productName, SUM(od.Quantity) [Number Sold]
	FROM Products p
	LEFT JOIN OrderDetails od
	ON p.ProductID = od.ProductID
	LEFT JOIN Categories c
	ON p.CategoryID = c.CategoryID
	WHERE c.CategoryName = @category
	GROUP BY p.productID, p.productName
END

EXEC itemsSold 'Confections'

--4.	Modify the stored procedure in #2/3 to make the parameter optional. If no 
	--parameter is passed in, then all products should be returned. Display the results 
	--for no parameter, as well as for the Beverages category.
ALTER PROCEDURE itemsSold (@category VARCHAR(15) = NULL) AS
BEGIN
	IF @category IS NOT NULL
	BEGIN
		SELECT p.productID, p.productName, SUM(od.Quantity) [Number Sold]
		FROM Products p
		LEFT JOIN OrderDetails od
		ON p.ProductID = od.ProductID
		LEFT JOIN Categories c
		ON p.CategoryID = c.CategoryID
		WHERE c.CategoryName = @category
		GROUP BY p.productID, p.productName
	END
	ELSE
	BEGIN
		SELECT p.productID, p.productName, SUM(od.Quantity) [Number Sold]
		FROM Products p
		LEFT JOIN OrderDetails od
		ON p.ProductID = od.ProductID
		GROUP BY p.productID, p.productName
	END
END

EXEC itemsSold 'Beverages'
EXEC itemsSold

--5.	Create a scalar function that calculates the order total for a given order. 
	--Use OrderID 10250 to test. The total should be $1552.60.
CREATE FUNCTION getOrderTotal (@orderID INT)
RETURNS DECIMAL(6,2) AS
BEGIN
	DECLARE @return DECIMAL(6,2)

	SELECT @return = SUM(od.Quantity * od.UnitPrice * (1 - od.Discount))
	FROM OrderDetails od
	WHERE od.OrderID = @orderID

	RETURN @return
END

SELECT dbo.getOrderTotal(10250) [Order Total]

--6.	Create a scalar function that returns the Customerís full address for a specific CustomerID.
CREATE FUNCTION getAddress (@id CHAR(5))
RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE @return VARCHAR(MAX)

	SELECT @return = CONCAT(c.Address + ' ', c.City + ' ', c.StateOrRegion + ' ', c.PostalCode)
	FROM Customers c
	WHERE c.CustomerID = @id

	RETURN @return
END

SELECT dbo.getAddress('ALFKI') [Address]