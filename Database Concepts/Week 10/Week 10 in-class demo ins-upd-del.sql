-- Track Ice Cream Flavors
Create table IceCreamFlavors(
FlavorID smallint identity(1,1),-- this is the primary key
FlavorName varchar(30) not null,
BrandName varchar(30) not null,
Description varchar(max),
Size varchar(15) not null,
IngredientList varchar(max),
Price decimal(5,2) check(Price >= 0),
AllergenInformation varchar(100) default 'N/A',
CaloriesPerServing smallint check(CaloriesPerServing > 0),
Distributor varchar(50),
IsGlutenFree bit not null default 0, -- don't allow nulls, but if no value is given, put a 0 in
RowCreationDate DateTime not null default GetDate(),
constraint pk_IceCreamFlavors primary key(FlavorID), -- forces the field to not allow nulls
constraint uk_IceCreamFlavors_FlavorName unique(FlavorName, BrandName) -- combination of flavorName and BrandName must be unique
)

-- When no columns are listed, all columns in the table must have data EXCEPT for identity columns
Insert into IceCreamFlavors
Values('Chocolate','Kemps','Chocolatey goodness', '1 pint', null, 9.99, null,1,'FedEx',1,GetDate())

select * from IceCreamFlavors

-- OR you can specify only the columns you want to put data into
-- at minimum, all not null fields must have values, unless they have default values listed in the table
Insert into IceCreamFlavors(FlavorName, BrandName, Size, Price, CaloriesPerServing)
Values('Cookies and Cream lite', 'Market Pantry', '1 gallon', 1.00, 70)

select * from IceCreamFlavors

Insert into IceCreamFlavors(FlavorName, BrandName, Size, Price, CaloriesPerServing, Description)
Values('Double Chocolate Fudge Brownie', 'Breyers', '4 liters', 36.00, 3500, 'All your day''s calories'),
('Cherry Garcia', 'Ben & Jerry''s', '1 pint', 3.99, 420, 'Chocolate vanilla Cherry something or other'),
('Swirl with the dragon cashews', 'Ben & Jerry''s', '1 pint', 7.99, 1000, 'Magical ice cream'),
('Unicorn', 'Market Pantry', '55 gal drum', 999.99, 1, 'Good luck finding it!'),
('Olive Oil Gelato', 'The Mario Bros', '1-up', 100.00, 50,'It''s a pipe dream'),
('Bowser''s Inferno', 'The Mario Bros', '1 shroom', 0.00, 500,'Fiery cinnamon ice cream'),
('Black Sesame', 'China town vendor', '1 scoop', 2.00, 170,'why? because.')

select * from IceCreamFlavors

-- For normalization purposes, move brandName to its own table
-- Create the brand table first
Create table IceCreamBrand(
BrandID smallint identity(1,1),
BrandName varchar(30) not null,
ContactName varchar(50),
ContactPhone varchar(20),
Constraint pk_IceCreamBrand primary key(BrandID)
)

-- write the query to identify the data to insert
Select Distinct BrandName
From IceCreamFlavors

--Now insert the data into the IceCreamBrands Table
Insert into IceCreamBrand(BrandName)
Select Distinct BrandName
From IceCreamFlavors

Select * From IceCreamBrand

-- This would update ALL The rows with this info.
-- DO NOT RUN THIS!!!!
Update IceCreamBrand
Set ContactName = 'Luigi',
ContactPhone = '(444) 555-1234'


Update IceCreamBrand
Set ContactName = 'Luigi',
ContactPhone = '(444) 555-1234'
where BrandID = 6

Select * From IceCreamBrand
Select * From IceCreamFlavors

-- add another column to IceCreamFlavors for the BrandID
Alter table IceCreamFlavors
Add BrandID smallint,
constraint fk_IceCreamFlavors_BrandID foreign key (BrandID)
References IceCreamBrand(BrandID)
-- the foreign key constraint is what forces the brandID to exist in the IceCreamBrands table
-- in order for the value to be added to the IceCreamFlavors table
-- this can be thought of as a "lookup" 

Select * From IceCreamBrand
Select * From IceCreamFlavors

-- Update the BrandID in the IceCreamFlavors table to have the correct corresponding BrandID from IceCreamBrands
-- Start by writing a select statement that Identifies the records affected and joins the tables

Select f.FlavorName, f.BrandName, b.BrandID, b.BrandName
From IceCreamFlavors f
Join IceCreamBrand b
on f.BrandName = b.BrandName

-- now write the update statement
-- there may be red squiggles under the BrandID because intellisense is confused
-- force intellisense to refresh by hitting ctrl+shift+r
Update IceCreamFlavors
Set BrandID = b.BrandID
From IceCreamFlavors f
Join IceCreamBrand b
on f.BrandName = b.BrandName

Select * From IceCreamFlavors

-- now verify the data
-- no rows returned means no problem, because all the brand names match
Select f.FlavorName, f.BrandName, b.BrandID, b.BrandName
From IceCreamFlavors f
Join IceCreamBrand b
on f.BrandID = b.BrandID
where f.BrandName <> b.BrandName 

Select * From IceCreamFlavors

-- remove the brandname column from the flavors table
-- this fails because there is a unique constraint on this column
-- must remove the constraint first
alter table IceCreamFlavors
Drop column BrandName

-- drop the constraint first, then drop the column name
alter table IceCreamFlavors
Drop constraint uk_IceCreamFlavors_FlavorName

alter table IceCreamFlavors
Drop column BrandName

Select * From IceCreamFlavors

-- recreate the spirit of the unique constraint
Alter table IceCreamFlavors
add constraint uk_IceCreamFlavors_FlavorName_Brand unique (FlavorName, BrandID)


Select * from IceCreamFlavors

-- remove the unicorn ice cream because we can't find it to sell it
-- write a select statement that identifies the rows to delete
select * from IceCreamFlavors
where flavorName = 'Unicorn'

-- change the select clause to be "delete"
Delete from IceCreamFlavors
where flavorName = 'Unicorn'

Select * from IceCreamBrand

-- Delete the row for Market Pantry as a brand

Select * from IceCreamBrand
Where BrandName = 'Market Pantry'

Delete from IceCreamBrand
Where BrandName = 'Market Pantry'

-- the above statement throws an error
-- the ID for market pantry is used in the flavors table
-- the foreign key constraint on the flavors table prevents the deletion
-- two options to fix this:
-- 1) Delete the row in the flavors table first, then delete the brand row
-- 2) Change the BrandID to a different brand for that flavor, then delete the brand

Select * from IceCreamFlavors 
where BrandID = 5

-- We're going to delete the row in the flavors table, then delete the row in teh brands table
-- DO NOT REMOVE FOREIGN KEYS!!! They are there for a reason!
Delete from IceCreamFlavors 
where BrandID = 5 -- Market Pantry

Delete from IceCreamBrand
Where BrandName = 'Market Pantry'


Select * from IceCreamBrand

Select * From IceCreamFlavors

-- Remove the tables
-- this must be done in a certain order
-- drop the flavors table first, then the brands table, because of the foreign key constraints
Drop table IceCreamFlavors
Drop table IceCreamBrand


