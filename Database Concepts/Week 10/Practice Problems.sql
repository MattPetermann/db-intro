CREATE TABLE Countries (
	CountryID INT IDENTITY(1,1),
	CountryName VARCHAR(20) NOT NULL,
	Continent VARCHAR(20),

	PRIMARY KEY(CountryID)
);

INSERT INTO Countries (CountryName)
	SELECT DISTINCT c.Country
	FROM Customers c
	UNION
	SELECT DISTINCT o.ShipCountry
	FROM Orders o

UPDATE Countries
	SET Continent = 'South America' 
	WHERE CountryName IN ('Argentina', 'Brazil', 'Venezuela')

UPDATE Countries
	SET Continent = 'Europe'
	WHERE CountryName IN ('Austria', 'Belgium', 'Denmark', 'Finland', 
		'France', 'Germany', 'Ireland', 'Italy', 'Norway', 'Poland', 
		'Portugal', 'Spain', 'Sweden', 'Switzerland', 'UK')

UPDATE Countries
	SET Continent = 'North America'
	WHERE CountryName IN ('Canada', 'Mexico', 'USA')

ALTER TABLE Orders
	ADD CountryID INT

ALTER TABLE Orders
	ADD CONSTRAINT FK_CountryID_Orders FOREIGN KEY (CountryID)
	REFERENCES Countries (CountryID)

ALTER TABLE Customers
	ADD CountryID INT
	
ALTER TABLE Customers
	ADD CONSTRAINT FK_CountryID_Customers FOREIGN KEY (CountryID)
	REFERENCES Countries (CountryID)

UPDATE Orders
	SET CountryID = c.CountryID
	FROM Orders o
	JOIN Countries c
	ON o.ShipCountry = c.CountryName

UPDATE Customers
	SET CountryID = c.CountryID
	FROM Orders o
	JOIN Countries c
	ON o.Country = c.CountryName

ALTER TABLE Orders
DROP CONSTRAINT fk_CountryID

ALTER TABLE Orders
DROP COLUMN ShipCountry

ALTER TABLE Customers
DROP CONSTRAINT fk_CountryID_Customers

ALTER TABLE Customers
DROP COLUMN Country