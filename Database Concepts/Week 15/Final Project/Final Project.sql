--Setup the category table
CREATE TABLE Category (
	CategoryID SMALLINT NOT NULL IDENTITY (1,1),
	CategoryName VARCHAR(20) NOT NULL,

	PRIMARY KEY(CategoryID)
);

--Flood the category table with category names
INSERT INTO Category (CategoryName) VALUES
	('Locks'),('Cleaners'),('Road Frames'),('Helmets'),('Mountain Frames'),
	('Bottles and Cages'),('Hydration Packs'),('Derailleurs'),('Bib-Shorts'),
	('Brakes'),('Jerseys'),('Road Bikes'),('Forks'),('Gloves'),('Socks'),('Fenders'),
	('Touring Frames'),('Caps'),('Pumps'),('Bike Stands'),('Chains'),('Handlebars'),
	('Shorts'),('Vests'),('Bike Racks'),('Wheels'),('Lights'),('Tights'),('Saddles'),
	('Mountain Bikes'),('Panniers'),('Touring Bikes'),('Pedals'),('Headsets'),
	('Cranksets'),('Tires and Tubes'),('Bottom Brackets');

--Link the info in the product table with a category id
ALTER TABLE Product
ADD CategoryID SMALLINT;

UPDATE Product
SET CategoryID = (
	SELECT c.CategoryID
	FROM Category c
	WHERE c.CategoryName = Product.Category
);

--Drop the category column from the product table
ALTER TABLE Product
DROP COLUMN Category;

--Setup the product line table
CREATE TABLE ProductLine (
	ProductLineID SMALLINT NOT NULL IDENTITY (1,1),
	ProductLine VARCHAR(20) NOT NULL,

	PRIMARY KEY(ProductLineID)
);

--Flood the product line table with product line names
INSERT INTO ProductLine (ProductLine) VALUES
	('Accessories'),('Components'),('Clothing'),('Bikes');

--Add product line id column to category table
ALTER TABLE Category
ADD ProductLineID SMALLINT;

--Add ids to match normalized data
UPDATE Category
SET ProductLineID =
	CASE 
		WHEN CategoryName IN ('Locks', 'Cleaners', 'Helmets', 'Bottles and Cages', 
			'Hydration Packs', 'Fenders', 'Pumps', 'Bike Stands', 'Bike Racks', 
			'Lights', 'Panniers', 'Tires and Tubes') THEN 1
		WHEN CategoryName IN ('Road Frames', 'Mountain Frames', 'Derailleurs', 
			'Brakes', 'Forks', 'Touring Frames', 'Chains', 'Handlebars', 'Wheels', 
			'Saddles', 'Pedals', 'Headsets', 'Cranksets', 'Bottom Brackets') THEN 2
		WHEN CategoryName IN ('Bib-Shorts', 'Jerseys', 'Gloves', 'Socks', 'Caps', 
			'Shorts', 'Vests', 'Tights') THEN 3	
		WHEN CategoryName IN ('Road Bikes', 'Mountain Bikes', 'Touring Bikes') THEN 4
	END;

--Create fields for full address
ALTER TABLE Customer
ADD FullAddress VARCHAR(MAX);

ALTER TABLE OrderHeader
ADD FullAddress VARCHAR(MAX);

--Set the full address fields
UPDATE Customer
SET FullAddress = CONCAT(Address, ' ', City, ' ', State, ' ', ZipCode);

UPDATE OrderHeader
SET FullAddress = CONCAT(ShipAddress, ' ', ShipCity, ' ', ShipState, ' ', ShipZipCode);

--Remove arbitrary values
ALTER TABLE Customer
DROP COLUMN Address, City, State, ZipCode;

ALTER TABLE OrderHeader
DROP COLUMN ShipAddress, ShipCity, ShipState, ShipZipCode;

--Create a table for address information
CREATE TABLE Address (
	AddressID SMALLINT NOT NULL IDENTITY(1,1),
	Address VARCHAR(MAX),

	PRIMARY KEY (AddressID)
);

--Copy the information from the customer table
INSERT INTO Address 
SELECT FullAddress FROM Customer;

--Copy the information from the order header table
INSERT INTO Address
SELECT FullAddress FROM OrderHeader;

--Get the unique addresses
CREATE TABLE Addresses (
	AddressID SMALLINT NOT NULL IDENTITY(1,1),
	Address VARCHAR(MAX),

	PRIMARY KEY (AddressID)
);

INSERT INTO Addresses
SELECT DISTINCT Address FROM Address;

--Remove table with duplicates
DROP TABLE Address;

--Add ids as link to addresses
ALTER TABLE Customer
ADD AddressID SMALLINT;

UPDATE Customer
SET AddressID = (
	SELECT AddressID
	FROM Addresses
	WHERE Addresses.Address = Customer.FullAddress
);

ALTER TABLE OrderHeader
ADD AddressID SMALLINT;

UPDATE OrderHeader
SET AddressID = (
	SELECT AddressID
	FROM Addresses
	WHERE Addresses.Address = OrderHeader.FullAddress
);

--Remove the address columns
ALTER TABLE Customer
DROP COLUMN FullAddress;

ALTER TABLE OrderHeader
DROP COLUMN FullAddress;

--Fix type mismatch error
ALTER TABLE SalesPromotion
DROP CONSTRAINT pk_SalesPromotion

ALTER TABLE SalesPromotion
ALTER COLUMN SalesPromotionID INT;

ALTER TABLE SalesPromotion
ADD CONSTRAINT pk_SalesPromotion
PRIMARY KEY (SalesPromotionID);

--Add foreign key constraints
ALTER TABLE Product
ADD CONSTRAINT fk_VendorID
FOREIGN KEY (VendorID) REFERENCES Vendor (VendorID);

ALTER TABLE OrderDetail
ADD CONSTRAINT fk_OrderID
FOREIGN KEY (OrderID) REFERENCES OrderHeader (OrderID);

ALTER TABLE OrderDetail
ADD CONSTRAINT fk_ProductID
FOREIGN KEY (ProductID) REFERENCES Product (ProductID);

ALTER TABLE OrderDetail
ADD CONSTRAINT fk_SalesPromotionID
FOREIGN KEY (SalesPromotionID) REFERENCES SalesPromotion (SalesPromotionID);

ALTER TABLE OrderHeader
ADD CONSTRAINT fk_CustomerID
FOREIGN KEY (CustomerID) REFERENCES Customer (CustomerID);

ALTER TABLE Customer
ADD CONSTRAINT fk_AddressID
FOREIGN KEY (AddressID) REFERENCES Addresses (AddressID);

ALTER TABLE OrderHeader
ADD CONSTRAINT fk_AddressID_OrderHeader
FOREIGN KEY (AddressID) REFERENCES Addresses (AddressID);

ALTER TABLE Product
ADD CONSTRAINT fk_CategoryID
FOREIGN KEY (CategoryID) REFERENCES Category (CategoryID);

ALTER TABLE Category
ADD CONSTRAINT fk_ProductLineID
FOREIGN KEY (ProductLineID) REFERENCES ProductLine (ProductLineID);