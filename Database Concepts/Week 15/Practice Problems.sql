--1.	Create a new role in your database called ProductUpdates. 
	--Grant this role permissions to be able to see and modify data 
	--in the products table. Add your test user account to the ProductUpdates 
	--role. Make sure the permissions work, then remove them from the role. 
CREATE ROLE ProductUpdates;
GRANT SELECT ON Products TO ProductUpdates;
GRANT ALTER ON Products TO ProductUpdates;

ALTER ROLE ProductUpdates
ADD MEMBER mpetermann2_TestUser

ALTER ROLE ProductUpdates
DROP MEMBER mpetermann2_TestUser

--2.	Create a new role called ReportDeveloper. Grant this role permissions 
	--to select from any table, and to create views, stored procedures, and 
	--user-defined functions. Add your test user account to the ReportDeveloper 
	--role. Make sure the permissions work.
CREATE ROLE ReportDeveloper
GRANT SELECT TO ReportDeveloper
GRANT CREATE VIEW TO ReportDeveloper
GRANT CREATE PROCEDURE TO ReportDeveloper
GRANT CREATE FUNCTION TO ReportDeveloper

ALTER ROLE ReportDeveloper
ADD MEMBER mpetermann2_TestUser

--3.	Remove the ability for the ReportDeveloper role to create user-defined 
	--functions. Make sure this permission works. 
REVOKE CREATE VIEW TO ReportDeveloper
DENY CREATE VIEW TO ReportDeveloper

--4.	Create the following stored procedure and then grant the ReportDeveloper 
	--role the ability to execute this stored procedure. Make sure the permissions 
	--work, then remove your test user account from the ReportDeveloper role. 
	--Paste the SQL to accomplish all of this.
GO CREATE PROCEDURE sp_GetActiveProductInfo AS
BEGIN
	SELECT p.ProductID, p.Productname, p.UnitPrice, p.WholesalePrice, p.UnitsInStock, p.UnitsOnOrder,
			s.SupplierID, s.CompanyName [SupplierName]
	FROM Products p
	JOIN Suppliers s
	ON p.SupplierID = s.SupplierID
	WHERE p.Discontinued = 0
END

GRANT EXEC ON sp_getActiveProductInfo TO ReportDeveloper
ALTER ROLE ReportDeveloper
DROP MEMBER mpetermann2_TestUser

--5.	Create a role called EmployeeDirectory. Allow this role to select 
	--from the employees table � however, they should not be able to select 
	--the employee�s address and home phone. Add your test user to the role 
	--and make sure the permissions work, then remove the test user from the role.
CREATE ROLE EmployeeDirectory
GRANT SELECT ON Employees TO EmployeeDirectory
DENY SELECT ON OBJECT::Employees (Address, HomePhone) TO EmployeeDirectory

ALTER ROLE EmployeeDirectory
ADD MEMBER mpetermann2_TestUser

ALTER ROLE EmployeeDirectory
DROP MEMBER mpetermann2_TestUser