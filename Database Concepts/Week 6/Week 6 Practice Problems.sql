--1.	Create a list of active employees. List their ID, First Initial + Last Name, and their hire date.
SELECT e.EmployeeID, LEFT(e.FirstName, 1) + ' ' + e.LastName [Name], e.HireDate
FROM Employees e;

--2.	Display all orders from 2015. Use a function to do this.
SELECT o.OrderID, o.OrderDate
FROM Orders o
WHERE DATEPART(year, o.OrderDate) = 2015;

--3.	For the most recent 30 orders, if the order was shipped, 
	--list the status as �Shipped on � plus the date it was shipped. 
	--If the order was not shipped, list �Not Shipped�. Include Order ID and Order Date.
SELECT TOP(30) o.OrderID, o.OrderDate, 
	IIF(o.ShippedDate IS NOT NULL, 'Shipped on ' + CONVERT(varchar, o.ShippedDate), 'Not shipped') [Status]
FROM Orders o
ORDER BY o.OrderDate desc;

--4.	Northwind Traders has decided to increase their retail prices. Write a query that 
	--shows the product name, the current price, and the new price. 
	--Make sure the new price is listed to 2 decimal points. Follow below guidelines on new retail price:
		--a.	If the category is beverages or dairy products, increase the price by 20%.
		--b.	If the category is meat/poultry or seafood, increase the price by 15%.
		--c.	If the Supplier is New Orleans Cajun Delights, increase the price by 8%.
		--d.	No other prices should be increased � list the current price.
CREATE VIEW UpdatedProducts AS
SELECT p.ProductID, p.ProductName, p.CategoryID [Category ID], p.SupplierID [Supplier ID],
	p.UnitPrice [Current Price], p.UnitPrice [New Price]
FROM Products p;

UPDATE UpdatedProducts
SET [New Price] = [Current Price] * 1.2
WHERE [Category ID] in (1, 4);

UPDATE UpdatedProducts
SET [New Price] = [Current Price] * 1.5
WHERE [Category ID] in (6, 8);

UPDATE UpdatedProducts
SET [New Price] = [Current Price] * 1.08
WHERE [Supplier ID] = 2;

SELECT u.ProductID, u.ProductName, CONVERT(Decimal(10, 2), u.[Current Price]) [Current Price], 
	CONVERT(DECIMAL(10, 2), u.[New Price]) [New Price]
FROM UpdatedProducts u;

--5.	Northwind Traders wants to make sure that all orders are shipped within 10 days. 
	--Display any orders that were not shipped within 10 days. Include the OrderID, the OrderDate, 
	--and the number of days it took before the order was shipped. Make this into a view 
	--so it can be referenced easily in the future.
CREATE VIEW LateProducts AS
SELECT o.OrderID, o.OrderDate, DATEDIFF(day, o.OrderDate, o.ShippedDate) [Days to Ship]
FROM Orders o;

SELECT *
FROM LateProducts l
WHERE l.[Days to Ship] > 10;