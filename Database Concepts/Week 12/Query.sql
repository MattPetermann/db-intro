-- Create the tables --

CREATE TABLE Students (
	StudentID SMALLINT IDENTITY(1,1),
	StudentName VARCHAR(50) NOT NULL,

	CONSTRAINT pk_studentID PRIMARY KEY (StudentID)
);

CREATE TABLE Products (
	ProductID SMALLINT IDENTITY(1,1),
	ProductName VARCHAR(20) NOT NULL,
	ItemsInStock SMALLINT NOT NULL,
	ItemsSold SMALLINT NOT NULL DEFAULT(0)

	CONSTRAINT pk_productID PRIMARY KEY (ProductID)
);

CREATE TABLE Sales (
	SaleID SMALLINT IDENTITY(1,1),
	TimeOfSale DATETIME NOT NULL,
	Quantity SMALLINT NOT NULL,
	ProductID SMALLINT NOT NULL,
	StudentID SMALLINT NOT NULL,

	CONSTRAINT pk_saleID PRIMARY KEY (SaleID),
	CONSTRAINT fk_productID FOREIGN KEY (ProductID)
		REFERENCES Products(ProductID),
	CONSTRAINT fk_studentID FOREIGN KEY (StudentID)
		REFERENCES Students (StudentID)	
);

-- Flood with data --

INSERT INTO Students VALUES (
	'Matt'
);

INSERT INTO Products VALUES (
	'Wings', 50, 0
);

INSERT INTO Sales VALUES (
	GETDATE(), 2, 1, 1
);